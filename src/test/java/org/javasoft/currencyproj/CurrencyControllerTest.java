package org.javasoft.currencyproj;

import lombok.val;
import org.javasoft.currencyproj.controller.UserDetailsController;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.client.ConversionResponse;
import org.javasoft.currencyproj.payload.client.RegisterRequest;
import org.javasoft.currencyproj.payload.client.RegisterResponse;
import org.javasoft.currencyproj.payload.currency.Symbols;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.javasoft.currencyproj.api.ClientApi.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(scripts = "classpath:populateDb.sql",  executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:dropData.sql",  executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class CurrencyControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    public TestRestTemplate testRestTemplate;


    private  String createUrlWithPort(String baseUrl ,String path) {
        final val stringBuilder = new StringBuilder();
        return stringBuilder.append("http://localhost")
                .append(":")
                .append(port)
                .append(baseUrl)
                .append(path)
                .toString();
    }

    private Util util;


    @Test
    public  void testConversionSuccess(){
        util = new Util();
        final val url = createUrlWithPort(USER_DETAILS_API,GET_TOKEN_API)+"?email=ay@te.com";
        final val responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, RegisterResponse.class);
        final val registerResponse = responseEntity.getBody();
        assertThat(registerResponse.getRegistrationId()).isNotBlank();
        assertThat(registerResponse.getToken()).isNotBlank();

        val registrationID = registerResponse.getRegistrationId();
        final val token = registerResponse.getToken();
        final val httpHeaders = util.buildHttpHeaders(registrationID, token);

        final val requestEntity = new HttpEntity<>(util.buildConversionRequest(), httpHeaders);
        val url2 = createUrlWithPort(CURRENCY_API,CONVERT_API);
        final val conversionResponseEntity = testRestTemplate.exchange(url2, HttpMethod.POST,requestEntity, ConversionResponse.class);
        assertThat(conversionResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        final val conversionResponse = conversionResponseEntity.getBody();
        assertNotNull(conversionResponse);
        final val conversionRate = conversionResponse.getConversionRate();
        final val expectedConversion = conversionResponse.getSourceAmount().multiply(conversionRate);
        assertTrue(expectedConversion.compareTo(conversionResponse.getDestinationAmount()) == 0);
    }

    @Test
    public  void testConversionFailure(){
        util = new Util();
        final val url = createUrlWithPort(USER_DETAILS_API,GET_TOKEN_API)+"?email=ay@te.com";
        final val responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, RegisterResponse.class);
        final val registerResponse = responseEntity.getBody();
        assertThat(registerResponse.getRegistrationId()).isNotBlank();
        assertThat(registerResponse.getToken()).isNotBlank();

        val registrationID = registerResponse.getRegistrationId();
        final val token = registerResponse.getToken();
        final val httpHeaders = util.buildHttpHeaders(registrationID, token);

        final val requestEntity = new HttpEntity<>(util.buildConversionRequestError(), httpHeaders);
        val url2 = createUrlWithPort(CURRENCY_API,CONVERT_API);
        final val conversionResponseEntity = testRestTemplate.exchange(url2, HttpMethod.POST,requestEntity, ConversionResponse.class);
        assertThat(conversionResponseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        final val conversionResponse = conversionResponseEntity.getBody();
        assertNotNull(conversionResponse);
    }

    @Test
    public  void testListOfCurrenciesSuccess(){
        util = new Util();
        final val url = createUrlWithPort(USER_DETAILS_API,GET_TOKEN_API)+"?email=ay@te.com";
        final val responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, RegisterResponse.class);
        final val registerResponse = responseEntity.getBody();
        assertThat(registerResponse.getRegistrationId()).isNotBlank();
        assertThat(registerResponse.getToken()).isNotBlank();

        val registrationID = registerResponse.getRegistrationId();
        final val token = registerResponse.getToken();
        final val httpHeaders = util.buildHttpHeaders(registrationID, token);

        final val requestEntity = new HttpEntity<>(httpHeaders);
        val url2 = createUrlWithPort(CURRENCY_API,SYMBOLS_API);
        val symbolListParamType = new ParameterizedTypeReference<List<Symbols>>() {};
        final val symbolListResponseEntity = testRestTemplate.exchange(url2, HttpMethod.GET,requestEntity, symbolListParamType);
        assertThat(symbolListResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        final val symbolListResponse = symbolListResponseEntity.getBody();
        assertThat(symbolListResponse).isNotEmpty();
    }


    @Test
    public void testRegisterSuccess() throws Exception {
        util = new Util();
        final val url = createUrlWithPort(USER_DETAILS_API,REGISTER_API);
        final val responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(util.buildRegisterRequest()), RegisterResponse.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
    }



}
