package org.javasoft.currencyproj;

import lombok.val;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.client.RegisterRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.javasoft.currencyproj.util.ConstUtil.REGISTRATION_ID_HEADER;
import static org.javasoft.currencyproj.util.ConstUtil.TOKEN_HEADER;

public class Util {

    public ConversionRequest buildConversionRequest(){
        final val conversionRequest = new ConversionRequest();
        conversionRequest.setDestinationCurrency("USD");
        conversionRequest.setSourceCurrency("EUR");
        conversionRequest.setSourceAmount(new BigDecimal(234));
        return conversionRequest;
    }

    public ConversionRequest buildConversionRequestError(){
        final val conversionRequest = new ConversionRequest();
        conversionRequest.setDestinationCurrency("");
        conversionRequest.setSourceCurrency("AWD");
        conversionRequest.setSourceAmount(new BigDecimal(234));
        return conversionRequest;
    }

    public RegisterRequest buildRegisterRequest(){
        final val registerRequest = new RegisterRequest();
        registerRequest.setEmail("a@a.com");
        registerRequest.setFirstName("Ay");
        registerRequest.setLastName("dg");
        return registerRequest;
    }

    public HttpHeaders buildHttpHeaders(String registrationID, String token){
        val requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.set(REGISTRATION_ID_HEADER,registrationID);
        requestHeaders.set(TOKEN_HEADER,token);
        return requestHeaders;
    }

    public RegisterRequest buildRegisterRequest2(){
        final val registerRequest = new RegisterRequest();
        registerRequest.setEmail("ailo@a.com");
        registerRequest.setFirstName("Ay");
        registerRequest.setLastName("dg");
        return registerRequest;
    }
}
