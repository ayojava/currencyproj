package org.javasoft.currencyproj;

import lombok.val;
import org.apache.commons.lang3.RandomStringUtils;
import org.javasoft.currencyproj.entity.UserDetailsEntity;
import org.javasoft.currencyproj.repository.UserDetailsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class UserDetailsRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Test
    void testUserDetailsIsCreatedSuccessfully(){
        final val userDetailsEntity = UserDetailsEntity.builder()
                .regNo(RandomStringUtils.randomAlphanumeric(6))
                .lastName("Ilori")
                .firstName("Gemma")
                .email("ayo@java.com")
                .build();
        final val userDetailsEntity_ = testEntityManager.persistAndFlush(userDetailsEntity);
        assertThat(userDetailsEntity_).isNotNull();
        final val userDetailsEntityOpt = userDetailsRepository.findById(userDetailsEntity_.getUserId());
        assertThat(userDetailsEntityOpt).isPresent();
        val userDetailsObj = userDetailsEntityOpt.get();
        assertThat(userDetailsObj.getEmail()).isEqualTo(userDetailsEntity.getEmail());
        assertThat(userDetailsObj.getFirstName()).isEqualTo(userDetailsEntity.getFirstName());
        assertThat(userDetailsObj.getLastName()).isEqualTo(userDetailsEntity.getLastName());
    }

    @Test
    void testDuplicateEmailThrowError(){
        final val userDetailsEntity = UserDetailsEntity.builder()
                .regNo(RandomStringUtils.randomAlphanumeric(6))
                .lastName("Ilori")
                .firstName("Gemma")
                .email("ayo@java.com")
                .build();
        final val userDetailsEntity2 = UserDetailsEntity.builder()
                .regNo(RandomStringUtils.randomAlphanumeric(6))
                .lastName("Ilori")
                .firstName("Gemma")
                .email("ayo@java.com")
                .build();
        final val userDetailsEntity_ = userDetailsRepository.save(userDetailsEntity);
        assertThat(userDetailsEntity_).isNotNull();
        assertThrows(Exception.class, () ->userDetailsRepository.save(userDetailsEntity2));
    }
}


