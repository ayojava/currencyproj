package org.javasoft.currencyproj.repository;

import org.javasoft.currencyproj.entity.UserDetailsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDetailsRepository extends CrudRepository<UserDetailsEntity, Long> {

    Optional<UserDetailsEntity> findByEmailAndRegNo(String email , String regNo);

    boolean existsByEmail(String email);

    Optional<UserDetailsEntity> findByEmail(String email);
}
