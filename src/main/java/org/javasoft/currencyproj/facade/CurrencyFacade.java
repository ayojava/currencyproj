package org.javasoft.currencyproj.facade;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.currencyproj.enums.ProviderEnum;
import org.javasoft.currencyproj.exception.CurrencyApiException;
import org.javasoft.currencyproj.exception.ErrorResponse;
import org.javasoft.currencyproj.factory.CurrencyProviderFactory;
import org.javasoft.currencyproj.mapper.ConversionMapper;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.client.ConversionResponse;
import org.javasoft.currencyproj.payload.currency.Symbols;
import org.javasoft.currencyproj.util.StartUpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;

@Service
public class CurrencyFacade {

    private CurrencyProviderFactory currencyProviderFactory;

    private StartUpUtil startUpUtil;

    private ConversionMapper conversionMapper;

    public ConversionResponse convertCurrency(ConversionRequest conversionRequest){
        final val currencyProviderIntf = currencyProviderFactory.getCurrencyProvider(getProvider(conversionRequest.getProviderName()));
        if(currencyProviderIntf == null) {
            final val errorResponse = ErrorResponse.builder()
                    .message("No provider found")
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
        validateCurrencies(conversionRequest.getSourceCurrency(), conversionRequest.getDestinationCurrency());
        final val conversionResponseDTO = currencyProviderIntf.doGetRequest(conversionRequest);
        return conversionMapper.doConversion(conversionResponseDTO.getRate(),conversionRequest);
    }

    public List<Symbols> getCurrencySymbols(){
        return startUpUtil.getSymbols();
    }

    private void validateCurrencies(String fromCurrency, String toCurrency){
        if(!startUpUtil.checkCurrencyCode(fromCurrency)){
            final val errorResponse = ErrorResponse.builder()
                    .message("Currency code " + fromCurrency + " not found ")
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
        if(!startUpUtil.checkCurrencyCode(toCurrency)){
            final val errorResponse = ErrorResponse.builder()
                    .message("Currency code " + toCurrency + " not found ")
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
    }

    @Autowired
    public void setConversionMapper(ConversionMapper conversionMapper) {
        this.conversionMapper = conversionMapper;
    }

    @Autowired
    public void setCurrencyProviderFactory(CurrencyProviderFactory currencyProviderFactory) {
        this.currencyProviderFactory = currencyProviderFactory;
    }

    private String getProvider(String providerName){
        return StringUtils.isBlank(providerName) ? ProviderEnum.findDefaultProvider().getProviderName() : providerName;
    }

    @Autowired
    public void setStartUpUtil(StartUpUtil startUpUtil) {
        this.startUpUtil = startUpUtil;
    }
}
