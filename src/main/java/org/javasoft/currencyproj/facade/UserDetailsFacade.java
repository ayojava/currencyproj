package org.javasoft.currencyproj.facade;

import lombok.val;
import org.javasoft.currencyproj.exception.CurrencyApiException;
import org.javasoft.currencyproj.exception.ErrorResponse;
import org.javasoft.currencyproj.mapper.UserDetailsMapper;
import org.javasoft.currencyproj.payload.client.RegisterRequest;
import org.javasoft.currencyproj.payload.client.RegisterResponse;
import org.javasoft.currencyproj.service.UserDetailsDbService;
import org.javasoft.currencyproj.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;
import static org.javasoft.currencyproj.util.ConstUtil.CLIENT_TOKEN_DURATION;

@Service
public class UserDetailsFacade {

    private UserDetailsMapper userDetailsMapper;

    private UserDetailsDbService userDetailsDbService;

    private JwtUtil jwtUtil;

    public RegisterResponse register(RegisterRequest registerRequest){
        if(userDetailsDbService.checkIfEmailExists(registerRequest.getEmail())){
            final val errorResponse = ErrorResponse.builder()
                    .message("Email already present in the database")
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
        final val userDetailsEntity = userDetailsMapper.mapUserDetailsEntity.apply(registerRequest);
        final val entity = userDetailsDbService.save(userDetailsEntity);
        return  RegisterResponse.builder()
                .registrationId(entity.getRegNo())
                .token(jwtUtil.createJWT(entity.getEmail(), CLIENT_TOKEN_DURATION))
                .build();
    }

    public RegisterResponse getUserToken(String email){
        final val userDetailsOpt = userDetailsDbService.findByEmail(email);
        if(userDetailsOpt.isEmpty()){
            final val errorResponse = ErrorResponse.builder()
                    .message("Email not found in the database")
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
        final val userDetailsEntity = userDetailsOpt.get();
        return  RegisterResponse.builder()
                .registrationId(userDetailsEntity.getRegNo())
                .token(jwtUtil.createJWT(userDetailsEntity.getEmail(), CLIENT_TOKEN_DURATION))
                .build();
    }

    @Autowired
    public void setUserDetailsMapper(UserDetailsMapper userDetailsMapper) {
        this.userDetailsMapper = userDetailsMapper;
    }

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setUserDetailsDbService(UserDetailsDbService userDetailsDbService) {
        this.userDetailsDbService = userDetailsDbService;
    }
}
