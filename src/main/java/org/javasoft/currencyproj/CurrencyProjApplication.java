package org.javasoft.currencyproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrencyProjApplication.class, args);
    }

}
