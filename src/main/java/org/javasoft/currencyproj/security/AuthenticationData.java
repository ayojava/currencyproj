package org.javasoft.currencyproj.security;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Builder
public class AuthenticationData {

    private String registrationId ;

    private String token;
}
