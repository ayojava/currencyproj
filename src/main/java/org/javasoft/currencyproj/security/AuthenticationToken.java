package org.javasoft.currencyproj.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;

import javax.security.auth.Subject;

public class AuthenticationToken extends AbstractAuthenticationToken {

    @Getter
    @Setter
    private  AuthenticationData authenticationData;

    public AuthenticationToken(AuthenticationData authenticationData) {
        super(null);
        this.authenticationData = authenticationData;
        setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return authenticationData;
    }

    @Override
    public boolean implies(Subject subject) {
        return super.implies(subject);
    }
}
