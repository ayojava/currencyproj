package org.javasoft.currencyproj.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.currencyproj.exception.ErrorResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.javasoft.currencyproj.exception.ErrorMessageIntf.AUTHENTICATION_ERROR_MSG;
import static org.javasoft.currencyproj.util.ConstUtil.REGISTRATION_ID_HEADER;
import static org.javasoft.currencyproj.util.ConstUtil.TOKEN_HEADER;

@Slf4j
public class AuthenticationFilter extends GenericFilterBean {

    private final AuthenticationManager authenticationManager;

    private  final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        val httpServletRequest = (HttpServletRequest) servletRequest;
        val httpServletResponse = (HttpServletResponse) servletResponse;
        try{
            handleAuthentication(httpServletRequest, httpServletResponse);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }catch (AuthenticationException authenticationException) {
            SecurityContextHolder.clearContext();
            log.error("Authentication Exception => " +authenticationException.getMessage());
            httpServletResponse.addHeader("Content-Type", "application/json");
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            final val errorResponse = ErrorResponse.builder()
                    .code(String.valueOf(HttpServletResponse.SC_UNAUTHORIZED))
                    .message(authenticationException.getMessage())
                    .build();
            httpServletResponse.getWriter().print(OBJECT_MAPPER.writeValueAsString(errorResponse));
        }catch (Exception exception){
            log.error(" Exception => " +exception.getMessage());
            httpServletResponse.addHeader("Content-Type", "application/json");
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            final val errorResponse = ErrorResponse.builder()
                    .code(String.valueOf(HttpServletResponse.SC_UNAUTHORIZED))
                    .message(AUTHENTICATION_ERROR_MSG)
                    .build();
            httpServletResponse.getWriter().print(OBJECT_MAPPER.writeValueAsString(errorResponse));
        }
    }

    private void handleAuthentication(HttpServletRequest httpServletRequest,  HttpServletResponse httpServletResponse) throws IOException, AuthenticationException {
        val registrationId = httpServletRequest.getHeader(REGISTRATION_ID_HEADER);
        val token = httpServletRequest.getHeader(TOKEN_HEADER);

        final val authenticationData = AuthenticationData.builder().token(token).registrationId(registrationId).build();

        final val authenticationToken = new AuthenticationToken(authenticationData);
        authenticationManager.authenticate(authenticationToken);
    }
}
