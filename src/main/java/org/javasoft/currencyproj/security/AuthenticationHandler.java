package org.javasoft.currencyproj.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.currencyproj.exception.AccessControlException;
import org.javasoft.currencyproj.exception.CurrencyApiException;
import org.javasoft.currencyproj.exception.ErrorResponse;
import org.javasoft.currencyproj.service.UserDetailsDbService;
import org.javasoft.currencyproj.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;
import static org.javasoft.currencyproj.exception.ErrorMessageIntf.*;

@Slf4j
@Component
public class AuthenticationHandler implements AuthenticationProvider {

    private JwtUtil jwtUtil;

    private UserDetailsDbService userDetailsDbService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        authentication.setAuthenticated(true);
        val authenticationToken = (AuthenticationToken) authentication;
        val authenticationData = authenticationToken.getAuthenticationData();
        validateClientAuthenticationData(authenticationData);

        validateUserToken(authenticationData);

        authenticationToken.setAuthenticationData(authenticationData);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        return authentication;
    }

    private void validateClientAuthenticationData(AuthenticationData authenticationData){
        if(StringUtils.isBlank(authenticationData.getRegistrationId())){
            val errorResponse = ErrorResponse.builder()
                    .message(USER_NAME_REQUIRED_MSG)
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }

        if(StringUtils.isBlank(authenticationData.getToken())){
            val errorResponse = ErrorResponse.builder()
                    .message(TOKEN_REQUIRED_MSG)
                    .code(CODE_999.getCode()).build();
            throw new CurrencyApiException(errorResponse);
        }
    }

    private void validateUserToken(AuthenticationData authenticationData){
        try{
            final val claims = jwtUtil.parseJWT(authenticationData.getToken());
            final val email = claims.getId();
            final val userDetailsOpt = userDetailsDbService.findUserDetails(email, authenticationData.getRegistrationId());
            if(!userDetailsOpt.isPresent()){
                throw new AccessControlException(INVALID_CREDENTIALS_MSG);
            }
        }catch (ExpiredJwtException eje) {
            log.error("Expired Token", eje);
            throw new AccessControlException(USER_TOKEN_EXPIRED_MSG);
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException ex) {
            log.error("Unknown error", ex);
            throw new AccessControlException(INVALID_TOKEN_MSG);
        }catch(Exception ex){
            log.error("Unknown error", ex);
            throw new AccessControlException(ex.getMessage());
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(AuthenticationToken.class);
    }

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setUserDetailsDbService(UserDetailsDbService userDetailsDbService) {
        this.userDetailsDbService = userDetailsDbService;
    }
}
