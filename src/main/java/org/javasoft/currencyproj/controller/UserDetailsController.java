package org.javasoft.currencyproj.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.currencyproj.facade.UserDetailsFacade;
import org.javasoft.currencyproj.payload.client.RegisterRequest;
import org.javasoft.currencyproj.payload.client.RegisterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.javasoft.currencyproj.api.ClientApi.*;

@Slf4j
@RestController
@RequestMapping(USER_DETAILS_API)
public class UserDetailsController {

    private UserDetailsFacade userDetailsFacade;

    @PostMapping(REGISTER_API)
    public ResponseEntity<RegisterResponse> register(@Valid @RequestBody RegisterRequest registerRequest){
        final val registerResponse = userDetailsFacade.register(registerRequest);
        return new ResponseEntity(registerResponse, HttpStatus.OK);
    }

    @GetMapping(GET_TOKEN_API)
    public ResponseEntity<RegisterResponse> getToken(@RequestParam String email){
        final val registerResponse = userDetailsFacade.getUserToken(email);
        return new ResponseEntity(registerResponse, HttpStatus.OK);
    }


    @Autowired
    public void setRegisterFacade(UserDetailsFacade userDetailsFacade) {
        this.userDetailsFacade = userDetailsFacade;
    }
}
