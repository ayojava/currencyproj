package org.javasoft.currencyproj.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.currencyproj.facade.CurrencyFacade;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.client.ConversionResponse;
import org.javasoft.currencyproj.payload.currency.Symbols;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.javasoft.currencyproj.api.ClientApi.*;

@Slf4j
@RestController
@RequestMapping(CURRENCY_API)
public class CurrencyController {

    private CurrencyFacade currencyFacade;

    @GetMapping(SYMBOLS_API)
    public ResponseEntity<List<Symbols>> getSymbols(){
        return new ResponseEntity(currencyFacade.getCurrencySymbols(), HttpStatus.OK);
    }

    @PostMapping(CONVERT_API)
    public ResponseEntity<ConversionResponse> convertCurrency(@Valid @RequestBody ConversionRequest conversionRequest){
        final val conversionResponse = currencyFacade.convertCurrency(conversionRequest);
        return new ResponseEntity(conversionResponse, HttpStatus.OK);
    }

    @Autowired
    public void setCurrencyFacade(CurrencyFacade currencyFacade) {
        this.currencyFacade = currencyFacade;
    }
}
