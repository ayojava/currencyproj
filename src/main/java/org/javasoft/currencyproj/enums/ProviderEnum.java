package org.javasoft.currencyproj.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

@Slf4j
@AllArgsConstructor
public enum ProviderEnum {

    EXCHANGE_RATE_API("erc", true);

    @Getter
    private String providerName ;

    @Getter
    private boolean isDefault ;

    public static ProviderEnum findDefaultProvider(){
       return Stream.of(ProviderEnum.values()).filter(ProviderEnum::isDefault).findFirst().get();
    }

    public static boolean isExchangeRateProvider(String providerName){
        return StringUtils.equalsIgnoreCase(EXCHANGE_RATE_API.providerName,providerName);
    }
}
