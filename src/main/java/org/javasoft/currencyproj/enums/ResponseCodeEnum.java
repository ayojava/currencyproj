package org.javasoft.currencyproj.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public enum ResponseCodeEnum {

    CODE_000("00","Approved or Completed Successfully" ),

    CODE_999("999","");

    @Getter
    private final String code;

    @Getter
    private final String description;
}
