package org.javasoft.currencyproj.mapper;

import lombok.val;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.client.ConversionResponse;
import org.javasoft.currencyproj.payload.exchangeapi.ConversionResponseDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.function.Function;

@Service
public class ConversionMapper {


    public ConversionResponse doConversion(BigDecimal conversionRate , ConversionRequest conversionRequest){
        return ConversionResponse.builder()
                .conversionRate(conversionRate)
                .destinationCurrency(conversionRequest.getDestinationCurrency())
                .sourceCurrency(conversionRequest.getSourceCurrency())
                .sourceAmount(conversionRequest.getSourceAmount())
                .destinationAmount(conversionRequest.getSourceAmount().multiply(conversionRate))
                .build();
    }
}
