package org.javasoft.currencyproj.mapper;

import org.apache.commons.lang3.RandomStringUtils;
import org.javasoft.currencyproj.entity.UserDetailsEntity;
import org.javasoft.currencyproj.payload.client.RegisterRequest;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class UserDetailsMapper {

    public Function<RegisterRequest, UserDetailsEntity> mapUserDetailsEntity = registerRequest -> UserDetailsEntity.builder().email(registerRequest.getEmail())
            .firstName(registerRequest.getFirstName())
            .lastName(registerRequest.getLastName())
            .regNo(RandomStringUtils.randomAlphanumeric(5).toUpperCase())
            .build();
}
