package org.javasoft.currencyproj.exception;

import lombok.Data;

@Data
public class ExchangeApiErrorResponse {

    private ErrorResponse error;

    private boolean success;
}
