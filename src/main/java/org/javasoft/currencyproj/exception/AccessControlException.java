package org.javasoft.currencyproj.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.authentication.AuthenticationServiceException;
import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;

@EqualsAndHashCode(callSuper = false)
public class AccessControlException extends AuthenticationServiceException {

    @Getter
    private ErrorResponse errorResponse;

    public AccessControlException(String msg) {
        super(msg);
        errorResponse = ErrorResponse.builder()
                .code(CODE_999.getCode())
                .message(msg)
                .build();
    }

    public AccessControlException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

