package org.javasoft.currencyproj.exception;

public interface ErrorMessageIntf {

    String EMPTY_FIELD_MSG = "Application Error : One or More Fields is empty or has an incorrect value ";

    String INVALID_FORMAT_MSG = "Application Error : One or More Fields has an invalid format";

    String USER_NAME_REQUIRED_MSG ="Application Error : username not sent in the request header";

    String TOKEN_REQUIRED_MSG ="Application Error : token not sent in the request header";

    String USER_TOKEN_EXPIRED_MSG ="Application Error : UserToken has expired , Kindly login again";

    String INVALID_TOKEN_MSG ="Application Error : Invalid Token   , Kindly login again";

    String INVALID_CREDENTIALS_MSG ="Application Error : Invalid credentials   , Kindly login again";

    String EXTERNAL_SERVICE_MSG ="Application Error :  could not connect to external service";

    String AUTHENTICATION_ERROR_MSG = "Application Error : An Error occurred while attempting authentications";

}
