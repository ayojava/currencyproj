package org.javasoft.currencyproj.exception;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CurrencyApiException extends RuntimeException{

    private ErrorResponse errorResponse;

    public void buildErrorResponse(String code , String info){
        errorResponse =  ErrorResponse.builder()
                .code(code)
                .message(info)
                .build();
    }
}
