package org.javasoft.currencyproj.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;
import static org.javasoft.currencyproj.exception.ErrorMessageIntf.EMPTY_FIELD_MSG;
import static org.javasoft.currencyproj.exception.ErrorMessageIntf.INVALID_FORMAT_MSG;

@Slf4j
@ControllerAdvice
public class GenericExceptionHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CurrencyApiException.class)
    public ErrorResponse handleCurrencyApiException(CurrencyApiException exception){
        return exception.getErrorResponse();
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse processBeanValidationError(BindException ex) {
        log.info("processBeanValidationError  => " + printStackTrace(ex));
        List<FieldError> error = ex.getBindingResult().getFieldErrors();

        if (!error.isEmpty()){
            for(FieldError anError : error){
                log.info("Field Name ::: [ " + anError.getField() + " ]  message ::: [ " + anError.getDefaultMessage() + " ] ");
            }
        }
        return new ErrorResponse(CODE_999.getCode(),EMPTY_FIELD_MSG);
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse processHttpMessageConversionException(HttpMessageConversionException ex) {

        if (ex.getCause() instanceof InvalidFormatException){
            InvalidFormatException invalidFormatException = (InvalidFormatException)ex.getCause();
            String fieldName = "";
            if (invalidFormatException.getPath() != null && invalidFormatException.getPath().get(0) != null){
                fieldName = invalidFormatException.getPath().get(0).getFieldName();
            }
            return new ErrorResponse(CODE_999.getCode(), "Unexpected value "+invalidFormatException.getValue().toString() + " sent for field "+fieldName);
        }
        return new ErrorResponse(CODE_999.getCode(),INVALID_FORMAT_MSG);
    }


    @ExceptionHandler(AccessControlException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorResponse processAccessControlError(AccessControlException accessControlException){
        return accessControlException.getErrorResponse();
    }


    private String printStackTrace(Exception exception) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        return sw.toString();
    }
}
