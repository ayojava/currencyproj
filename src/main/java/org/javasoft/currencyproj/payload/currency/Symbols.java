package org.javasoft.currencyproj.payload.currency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Symbols {

    private String symbol;

    private String country;
}
