package org.javasoft.currencyproj.payload.exchangeapi;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Info {

    private long timestamp;

    private BigDecimal rate;
}
