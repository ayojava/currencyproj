package org.javasoft.currencyproj.payload.exchangeapi;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Query {

    private String from ;

    private String to;

    private BigDecimal amount;
}
