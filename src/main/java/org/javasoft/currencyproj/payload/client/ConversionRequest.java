package org.javasoft.currencyproj.payload.client;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class ConversionRequest {

    private String providerName;

    @NotBlank(message = "source currency is required")
    private String sourceCurrency;

    @NotBlank(message = "destination currency is required")
    private String destinationCurrency;

    @DecimalMin(value = "0.00", inclusive = false ,message = "Amount must be greater than zero")
    private BigDecimal sourceAmount;
}
