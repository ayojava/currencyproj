package org.javasoft.currencyproj.payload.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConversionResponse {

    private String sourceCurrency;

    private String destinationCurrency;

    private BigDecimal sourceAmount;

    private BigDecimal destinationAmount;

    private BigDecimal conversionRate;

}
