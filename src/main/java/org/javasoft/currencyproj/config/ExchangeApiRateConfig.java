package org.javasoft.currencyproj.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@ConfigurationProperties(prefix = "erc")
public class ExchangeApiRateConfig {

    @NotBlank
    private String apiKey ;

    @NotBlank
    private String baseUrl;

    @NotBlank
    private String name;

    private boolean active;

}
