package org.javasoft.currencyproj.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.javasoft.currencyproj.service.ExchangeApiHttpClient;
import org.javasoft.currencyproj.service.ExchangeApiRestTemplateFactory;
import org.javasoft.currencyproj.util.JsonUtil;
import org.javasoft.currencyproj.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.TimeZone;

@Configuration
public class BeanConfig {

    private RestTemplateBuilder restTemplateBuilder;

    @Bean
    public ExchangeApiHttpClient exchangeApiHttpClient(){
        final val exchangeApiHttpClient = new ExchangeApiHttpClient(new ExchangeApiRestTemplateFactory(restTemplateBuilder).getRestTemplate());
        exchangeApiHttpClient.setJsonUtil(jsonUtil());
        return exchangeApiHttpClient;
    }

    @Bean
    public JwtUtil jwtConfig(CurrencyConfig currencyConfig){
        final val jwtConfig = new JwtUtil();
        jwtConfig.setKey(currencyConfig.getJwtKey());
        jwtConfig.setIssuer(currencyConfig.getJwtIssuer());
        jwtConfig.setSubject(currencyConfig.getJwtSubject());
        return jwtConfig;
    }
    
    private JsonUtil jsonUtil(){
        final val jsonUtil = new JsonUtil();
        jsonUtil.setObjectMapper(defaultObjectMapper());
        jsonUtil.setJsonParser(JsonParserFactory.getJsonParser());
        return jsonUtil;
    }


    private ObjectMapper defaultObjectMapper(){
        TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("Africa/Lagos");
        ObjectMapper OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.setTimeZone(DEFAULT_TIMEZONE);
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return OBJECT_MAPPER;
    }

    @Autowired
    public void setRestTemplateBuilder(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }
}
