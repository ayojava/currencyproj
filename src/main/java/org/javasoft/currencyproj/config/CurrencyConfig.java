package org.javasoft.currencyproj.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@ConfigurationProperties(prefix = "cfg")
public class CurrencyConfig {

    @NotBlank
    private String jwtKey;

    @NotBlank
    private String jwtIssuer;

    @NotBlank
    private String  jwtSubject;


}
