package org.javasoft.currencyproj.factory;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.currencyproj.config.ExchangeApiRateConfig;
import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.exchangeapi.ConversionResponseDTO;
import org.javasoft.currencyproj.service.ExchangeApiHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExchangeApiProvider implements CurrencyProviderIntf{

    private ExchangeApiRateConfig exchangeApiRateConfig;

    private ExchangeApiHttpClient exchangeApiHttpClient;

    public ConversionResponseDTO doGetRequest(ConversionRequest conversionRequest) {
        final val path = new StringBuilder().append(exchangeApiRateConfig.getBaseUrl())
                .append("/2013-12-24")
                .append("?access_key=")
                .append(exchangeApiRateConfig.getApiKey())
                .append("&base=")
                .append(conversionRequest.getSourceCurrency())
                .append("&symbols=")
                .append(conversionRequest.getDestinationCurrency())
                .toString();
        /*
        Hack to handle response from the api since we are working with historical data response
        {"success":true,"timestamp":1387929599,"historical":true,"base":"EUR","date":"2013-12-24","rates":{"USD":1.367761}}
         */
        exchangeApiHttpClient.setDestinationCurrency(conversionRequest.getDestinationCurrency());
        return exchangeApiHttpClient.doGetRequest(path);
    }

    @Autowired
    public void setExchangeApiRateConfig(ExchangeApiRateConfig exchangeApiRateConfig) {
        this.exchangeApiRateConfig = exchangeApiRateConfig;
    }

    @Autowired
    public void setExchangeApiHttpClient(ExchangeApiHttpClient exchangeApiHttpClient) {
        this.exchangeApiHttpClient = exchangeApiHttpClient;
    }

    @Override
    public boolean isActive() {
        return exchangeApiRateConfig.isActive();
    }

    @Override
    public String getName() {
        return exchangeApiRateConfig.getName();
    }




}
