package org.javasoft.currencyproj.factory;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.currencyproj.enums.ProviderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyProviderFactory {

    private List<CurrencyProviderIntf> currencyProviderList;

    public CurrencyProviderIntf getCurrencyProvider(String providerName){
        for(CurrencyProviderIntf aCurrencyProvider : currencyProviderList){
            if(StringUtils.equalsIgnoreCase(aCurrencyProvider.getName(), providerName)){
                if(ProviderEnum.isExchangeRateProvider(providerName)){
                    ExchangeApiProvider exchangeApiProvider = (ExchangeApiProvider) aCurrencyProvider;
                   return exchangeApiProvider;
                }
                return null;
            }
        }
        return null;
    }

    @Autowired
    public void setCurrencyProviderList(List<CurrencyProviderIntf> currencyProviderList) {
        this.currencyProviderList = currencyProviderList;
    }
}
