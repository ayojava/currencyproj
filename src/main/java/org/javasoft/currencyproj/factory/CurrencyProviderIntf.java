package org.javasoft.currencyproj.factory;

import org.javasoft.currencyproj.payload.client.ConversionRequest;
import org.javasoft.currencyproj.payload.exchangeapi.ConversionResponseDTO;

public interface CurrencyProviderIntf {

    boolean isActive();

    String getName();

    ConversionResponseDTO doGetRequest(ConversionRequest conversionRequest);

}
