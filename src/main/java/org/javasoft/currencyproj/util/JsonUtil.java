package org.javasoft.currencyproj.util;
import lombok.val;
import org.springframework.boot.json.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JsonUtil {

    @Getter @Setter
    private ObjectMapper objectMapper;

    @Setter
    private JsonParser jsonParser;

    public <T> T convertStringToObject(String val , Class<T> responseType){
        try{
            return objectMapper.readValue(val , responseType);
        }catch(Exception ex){
            log.error("Conversion Error :::: ",ex);
        }
        return null;
    }

    public BigDecimal parseRate(String jsonString,String destinationCurrency){
        final val map = jsonParser.parseMap(jsonString);
        final val rates = (HashMap)map.get("rates");
        return new BigDecimal((Double) rates.get(destinationCurrency));
    }
}
