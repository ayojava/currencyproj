package org.javasoft.currencyproj.util;

public interface ConstUtil {

    int CLIENT_TOKEN_DURATION = 1000 * 60 * 60; //1 hour

    String REGISTRATION_ID_HEADER ="registrationId";

    String TOKEN_HEADER = "token";
}
