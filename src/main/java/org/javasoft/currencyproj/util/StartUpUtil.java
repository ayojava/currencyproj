package org.javasoft.currencyproj.util;


import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.currencyproj.payload.currency.Symbols;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class StartUpUtil {

   private Map<String,String> symbols;

   @PostConstruct
   public void init(){
       symbols = new HashMap<>();
       symbols.put("AED", "United Arab Emirates Dirham");
               symbols.put( "AFN", "Afghan Afghani");
               symbols.put("ALL","Albanian Lek");
               symbols.put( "AMD", "Armenian Dram");
               symbols.put("ANG", "Netherlands Antillean Guilder");
               symbols.put("AOA", "Angolan Kwanza");
               symbols.put("ARS", "Argentine Peso");
               symbols.put("AUD", "Australian Dollar");
               symbols.put("AWG", "Aruban Florin");
               symbols.put("AZN", "Azerbaijani Manat");
               symbols.put("BAM", "Bosnia-Herzegovina Convertible Mark");
               symbols.put("BBD", "Barbadian Dollar");
               symbols.put("BDT", "Bangladeshi Taka");
               symbols.put("BGN", "Bulgarian Lev");
               symbols.put("BHD", "Bahraini Dinar");
               symbols.put("BIF", "Burundian Franc");
               symbols.put("BMD", "Bermudan Dollar");
               symbols.put("BND", "Brunei Dollar");
               symbols.put("BOB", "Bolivian Boliviano");
               symbols.put("BRL", "Brazilian Real");
               symbols.put("BSD", "Bahamian Dollar");
               symbols.put("BTC", "Bitcoin");
               symbols.put("BTN", "Bhutanese Ngultrum");
               symbols.put("BWP", "Botswanan Pula");
               symbols.put("BYN", "New Belarusian Ruble");
               symbols.put("BYR", "Belarusian Ruble");
               symbols.put("BZD", "Belize Dollar");
               symbols.put("CAD", "Canadian Dollar");
               symbols.put("CDF", "Congolese Franc");
               symbols.put("CHF", "Swiss Franc");
               symbols.put("CLF", "Chilean Unit of Account (UF)");
               symbols.put("CLP", "Chilean Peso");
               symbols.put("CNY", "Chinese Yuan");
               symbols.put("COP", "Colombian Peso");
               symbols.put("CRC", "Costa Rican Colón");
               symbols.put("CUC", "Cuban Convertible Peso");
               symbols.put("CUP", "Cuban Peso");
               symbols.put("CVE", "Cape Verdean Escudo");
               symbols.put("CZK", "Czech Republic Koruna");
               symbols.put("DJF", "Djiboutian Franc");
               symbols.put("DKK", "Danish Krone");
               symbols.put("DOP", "Dominican Peso");
               symbols.put("DZD", "Algerian Dinar");
               symbols.put("EGP", "Egyptian Pound");
               symbols.put("ERN", "Eritrean Nakfa");
               symbols.put("ETB", "Ethiopian Birr");
               symbols.put("EUR", "Euro");
               symbols.put("FJD", "Fijian Dollar");
               symbols.put("FKP", "Falkland Islands Pound");
               symbols.put("GBP", "British Pound Sterling");
               symbols.put("GEL", "Georgian Lari");
               symbols.put("GGP", "Guernsey Pound");
               symbols.put("GHS", "Ghanaian Cedi");
               symbols.put("GIP", "Gibraltar Pound");
               symbols.put("GMD", "Gambian Dalasi");
               symbols.put("GNF", "Guinean Franc");
               symbols.put("GTQ", "Guatemalan Quetzal");
               symbols.put("GYD", "Guyanaese Dollar");
               symbols.put("HKD", "Hong Kong Dollar");
               symbols.put("HNL", "Honduran Lempira");
               symbols.put("HRK", "Croatian Kuna");
               symbols.put("HTG", "Haitian Gourde");
               symbols.put("HUF", "Hungarian Forint");
               symbols.put("IDR", "Indonesian Rupiah");
               symbols.put("ILS", "Israeli New Sheqel");
               symbols.put("IMP", "Manx pound");
               symbols.put("INR", "Indian Rupee");
               symbols.put("IQD", "Iraqi Dinar");
               symbols.put("IRR", "Iranian Rial");
               symbols.put("ISK", "Icelandic Króna");
               symbols.put("JEP", "Jersey Pound");
               symbols.put("JMD", "Jamaican Dollar");
               symbols.put("JOD", "Jordanian Dinar" );
               symbols.put("JPY", "Japanese Yen");
               symbols.put("KES", "Kenyan Shilling");
               symbols.put("KGS", "Kyrgystani Som");
               symbols.put("KHR", "Cambodian Riel");
               symbols.put("KMF", "Comorian Franc");
               symbols.put("KPW", "North Korean Won");
               symbols.put("KRW", "South Korean Won");
               symbols.put("KWD", "Kuwaiti Dinar");
               symbols.put("KYD", "Cayman Islands Dollar");
               symbols.put("KZT", "Kazakhstani Tenge");
               symbols.put("LAK", "Laotian Kip");
               symbols.put("LBP", "Lebanese Pound");
               symbols.put("LKR", "Sri Lankan Rupee");
               symbols.put("LRD", "Liberian Dollar");
               symbols.put("LSL", "Lesotho Loti");
               symbols.put("LTL", "Lithuanian Litas");
               symbols.put("LVL", "Latvian Lats");
               symbols.put("LYD", "Libyan Dinar");
               symbols.put("MAD", "Moroccan Dirham");
               symbols.put("MDL", "Moldovan Leu");
               symbols.put("MGA", "Malagasy Ariary");
               symbols.put("MKD", "Macedonian Denar");
               symbols.put("MMK", "Myanma Kyat");
               symbols.put("MNT", "Mongolian Tugrik");
               symbols.put("MOP", "Macanese Pataca");
               symbols.put("MRO", "Mauritanian Ouguiya");
               symbols.put("MUR", "Mauritian Rupee");
               symbols.put("MVR", "Maldivian Rufiyaa");
               symbols.put("MWK", "Malawian Kwacha");
               symbols.put("MXN", "Mexican Peso");
               symbols.put("MYR", "Malaysian Ringgit");
               symbols.put("MZN", "Mozambican Metical");
               symbols.put("NAD", "Namibian Dollar");
               symbols.put("NGN", "Nigerian Naira");
               symbols.put("NIO", "Nicaraguan Córdoba");
               symbols.put("NOK", "Norwegian Krone");
               symbols.put("NPR", "Nepalese Rupee");
               symbols.put( "NZD", "New Zealand Dollar");
               symbols.put("OMR", "Omani Rial");
               symbols.put("PAB", "Panamanian Balboa");
               symbols.put("PEN", "Peruvian Nuevo Sol");
               symbols.put("PGK", "Papua New Guinean Kina");
               symbols.put("PHP", "Philippine Peso");
               symbols.put("PKR", "Pakistani Rupee");
               symbols.put("PLN", "Polish Zloty");
               symbols.put("PYG", "Paraguayan Guarani");
               symbols.put("QAR", "Qatari Rial");
               symbols.put("RON", "Romanian Leu");
               symbols.put("RSD", "Serbian Dinar");
               symbols.put("RUB", "Russian Ruble");
               symbols.put("RWF", "Rwandan Franc");
               symbols.put("SAR", "Saudi Riyal");
               symbols.put("SBD", "Solomon Islands Dollar");
               symbols.put("SCR", "Seychellois Rupee");
               symbols.put("SDG", "Sudanese Pound");
               symbols.put("SEK", "Swedish Krona");
               symbols.put("SGD", "Singapore Dollar");
               symbols.put("SHP", "Saint Helena Pound");
               symbols.put("SLL", "Sierra Leonean Leone");
               symbols.put("SOS", "Somali Shilling");
               symbols.put("SRD", "Surinamese Dollar");
               symbols.put("STD", "São Tomé and Príncipe Dobra");
               symbols.put("SVC", "Salvadoran Colón");
               symbols.put("SYP", "Syrian Pound");
               symbols.put("SZL", "Swazi Lilangeni");
               symbols.put("THB", "Thai Baht");
               symbols.put("TJS", "Tajikistani Somoni");
               symbols.put("TMT", "Turkmenistani Manat");
               symbols.put("TND", "Tunisian Dinar");
               symbols.put("TOP", "Tongan Paʻanga");
               symbols.put("TRY", "Turkish Lira");
               symbols.put("TTD", "Trinidad and Tobago Dollar");
               symbols.put("TWD", "New Taiwan Dollar");
               symbols.put("TZS", "Tanzanian Shilling");
               symbols.put("UAH", "Ukrainian Hryvnia");
               symbols.put( "UGX", "Ugandan Shilling");
               symbols.put("USD", "United States Dollar");
               symbols.put("UYU", "Uruguayan Peso");
               symbols.put("UZS", "Uzbekistan Som");
               symbols.put("VEF", "Venezuelan Bolívar Fuerte");
               symbols.put("VND", "Vietnamese Dong");
               symbols.put("VUV", "Vanuatu Vatu");
               symbols.put("WST", "Samoan Tala");
               symbols.put("XAF", "CFA Franc BEAC");
               symbols.put("XAG", "Silver (troy ounce)");
               symbols.put("XAU", "Gold (troy ounce)");
               symbols.put("XCD", "East Caribbean Dollar");
               symbols.put("XDR", "Special Drawing Rights");
               symbols.put("XOF", "CFA Franc BCEAO");
               symbols.put("XPF", "CFP Franc");
               symbols.put("YER", "Yemeni Rial");
               symbols.put("ZAR", "South African Rand");
               symbols.put("ZMK", "Zambian Kwacha (pre-2013)");
               symbols.put("ZMW", "Zambian Kwacha");
               symbols.put("ZWL", "Zimbabwean Dollar");
   }

   public boolean checkCurrencyCode(String currencyCode){
       return symbols.containsKey(currencyCode);
   }

   public List<Symbols> getSymbols(){
       return symbols.entrySet()
               .stream()
               .map(aSymbol -> new Symbols(aSymbol.getKey(), aSymbol.getValue()))
               .collect(Collectors.toList());
   }
}
