package org.javasoft.currencyproj.service;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.currencyproj.exception.CurrencyApiException;
import org.javasoft.currencyproj.exception.ExchangeApiErrorResponse;
import org.javasoft.currencyproj.payload.exchangeapi.ConversionResponseDTO;
import org.javasoft.currencyproj.util.JsonUtil;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import static org.javasoft.currencyproj.enums.ResponseCodeEnum.CODE_999;
import static org.javasoft.currencyproj.exception.ErrorMessageIntf.EXTERNAL_SERVICE_MSG;

@Slf4j
public class ExchangeApiHttpClient {

    private final RestTemplate restTemplate;

    @Setter
    private JsonUtil jsonUtil;

    @Setter
    //not necessary , if we were to get the api access to get converted currency rate
    private String destinationCurrency;

    public ExchangeApiHttpClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /*
        Hack to handle response from the api since we are working with historical data response
        {"success":true,"timestamp":1387929599,"historical":true,"base":"EUR","date":"2013-12-24","rates":{"USD":1.367761}}
         */
    public ConversionResponseDTO doGetRequest(String path){
        try{
            final val responseEntity = restTemplate.exchange(path, HttpMethod.GET, null, String.class);
            final val statusCode = responseEntity.getStatusCode();
            if(statusCode.is2xxSuccessful()){
                return ConversionResponseDTO.builder()
                        .rate(jsonUtil.parseRate(responseEntity.getBody(),destinationCurrency))
                        .build();
            }else if(statusCode.is4xxClientError() || statusCode.is5xxServerError()){
                final val exchangeApiErrorResponse = jsonUtil.convertStringToObject(responseEntity.getBody(), ExchangeApiErrorResponse.class);
                final val errorResponse = exchangeApiErrorResponse.getError();
                throw new CurrencyApiException(errorResponse);
            }
        }catch (Exception exception){
            final val currencyApiException = new CurrencyApiException();
            log.error("Error while connecting to external service " , exception);
            currencyApiException.buildErrorResponse(CODE_999.getCode(), EXTERNAL_SERVICE_MSG);
            throw currencyApiException;
        }
        return null;
    }
}
