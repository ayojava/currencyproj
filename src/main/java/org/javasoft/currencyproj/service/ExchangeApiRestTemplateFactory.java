package org.javasoft.currencyproj.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

public class ExchangeApiRestTemplateFactory {

    private final RestTemplateBuilder restTemplateBuilder;

    public ExchangeApiRestTemplateFactory(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    public RestTemplate getRestTemplate(){
        return restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(10))
                .setReadTimeout(Duration.ofSeconds(10))
                .build();
    }
}
