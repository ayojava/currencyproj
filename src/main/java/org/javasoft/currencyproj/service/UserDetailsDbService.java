package org.javasoft.currencyproj.service;

import org.javasoft.currencyproj.entity.UserDetailsEntity;
import org.javasoft.currencyproj.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsDbService {

    private UserDetailsRepository userDetailsRepository;

    public Optional<UserDetailsEntity> findUserDetails(String email, String regNo  ){
        return userDetailsRepository.findByEmailAndRegNo(email,regNo);
    }

    public boolean checkIfEmailExists(String email){
        return userDetailsRepository.existsByEmail(email);
    }

    public Optional<UserDetailsEntity> findByEmail(String email){
        return userDetailsRepository.findByEmail(email);
    }

    public UserDetailsEntity save(UserDetailsEntity userDetailsEntity){
        return userDetailsRepository.save(userDetailsEntity);
    }
    @Autowired
    public void setUserDetailsRepository(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }
}
