package org.javasoft.currencyproj.api;

public interface ClientApi {

    String CURRENCY_API="/api/v1/currency";

    String CONVERT_API = "/convert";

    String SYMBOLS_API = "/getSymbols";

    String REGISTER_API = "/register";

    String USER_DETAILS_API = "/api/v1/userDetails";

    String GET_TOKEN_API = "/getToken";
}
