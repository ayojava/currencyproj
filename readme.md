# Currency Converter

* Below are the instructions to access the api on heroku
* 
** REGISTER ENDPOINT 
* register by sending a POST request to
* https://proj-currency.herokuapp.com/api/v1/userDetails/register and passing in "firstName","lastName" and "email"
```json
{
    "firstName" : "Ayo",
    "lastName" : "ilori",
    "email" :"ayo@gmail.com"
}
```
The response will be 
```json
{
    "registrationId": "XFUB0",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJheW9AZ21haWwuY29tIiwiaWF0IjoxNjM3NDM3Mzc1LCJzdWIiOiJ0ZXN0U3ViamVjdCIsImlzcyI6InRlc3RJc3N1ZXIiLCJleHAiOjE2Mzc0NDA5NzV9.0KugAERHIr8XaqIHkhiBI31EmLNErq4PeSbuORIAqm8"
}
```

The token in the above is valid for 1 hour and will be used to access the other endpoints

** GET TOKEN  ENDPOINT 
* This endpoint is used to retrieve a new token by sending a 
* GET request to https://proj-currency.herokuapp.com/api/v1/userDetails/getToken?email=[REGISTERED-EMAIL]
* The response will be
```json
{
    "registrationId": "XFUB0",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJheW9AZ21haWwuY29tIiwiaWF0IjoxNjM3NTA1NzA2LCJzdWIiOiJ0ZXN0U3ViamVjdCIsImlzcyI6InRlc3RJc3N1ZXIiLCJleHAiOjE2Mzc1MDkzMDZ9.WUrDsZn3z1VHIH7574rWv-2F_y0GMTa-VRf6pJllo6s"
}
```

** GET SYMBOLS
* This endpoint retrieves all the possible currency codes by making a 
* GET request to https://proj-currency.herokuapp.com/api/v1/currency/getSymbols
* Add the following as headers before making the request.
```css
token : eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJheW9AZ21haWwuY29tIiwiaWF0IjoxNjM3NTA1NzA2LCJzdWIiOiJ0ZXN0U3ViamVjdCIsImlzcyI6InRlc3RJc3N1ZXIiLCJleHAiOjE2Mzc1MDkzMDZ9.WUrDsZn3z1VHIH7574rWv-2F_y0GMTa-VRf6pJllo6s
registrationId : XFUB0
```
This would return 
```json
[
    {
        "symbol": "FJD",
        "country": "Fijian Dollar"
    },
    {
        "symbol": "MXN",
        "country": "Mexican Peso"
    },
    {
        "symbol": "STD",
        "country": "São Tomé and Príncipe Dobra"
    }
  }
```
* NB : The records will be more than the above

** CONVERT 
* This is used to convert the amount  from one currency to another by sending a POST request to 
* https://proj-currency.herokuapp.com/api/v1/currency/convert
* Add the following as headers before making the request.
```css
token : eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJheW9AZ21haWwuY29tIiwiaWF0IjoxNjM3NTA1NzA2LCJzdWIiOiJ0ZXN0U3ViamVjdCIsImlzcyI6InRlc3RJc3N1ZXIiLCJleHAiOjE2Mzc1MDkzMDZ9.WUrDsZn3z1VHIH7574rWv-2F_y0GMTa-VRf6pJllo6s
registrationId : XFUB0
```
The request payload is
```json
{
    "sourceCurrency" : "EUR",
    "destinationCurrency" :"USD",
    "sourceAmount":4500
}
```

The response payload is

```json
{
    "sourceCurrency": "EUR",
    "destinationCurrency": "USD",
    "sourceAmount": 4500,
    "destinationAmount": 6154.9245000000000205275796361092943698167800903320312500,
    "conversionRate": 1.3677610000000000045616843635798431932926177978515625
}
```

* To run the application locally , edit the application-dev.yml and set the "active profile " to "dev"